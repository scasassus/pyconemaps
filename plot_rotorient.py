import matplotlib
import matplotlib.pyplot as plt
import numpy as np
#from pylab import *
import matplotlib.colors as colors
import re


import LogResults


workdir='work_gmom_8_werrmap/'

bmaj=0.054 # arcsec

distance=135.77 # GAIA  2018

nametag='im_g_v0'


#######################################################################
# no further user input
######################################################################
PlotContinuumGaps=False


workdir_fixincPA=re.sub('/$','_fixPAinc/',workdir)


file_profile=workdir+nametag+'_radial_profile.dat'
file_profile_fixincPA=workdir_fixincPA+nametag+'_radial_profile.dat'
fileout_fig=workdir_fixincPA+'fig_rotorient_surfandmid_linear_dev.pdf'

print("loading file_profiles",file_profile," ",file_profile_fixincPA)

allprofiles=np.loadtxt(file_profile,unpack=True)
allprofiles_fixincPA=np.loadtxt(file_profile_fixincPA,unpack=True)

print("length allprofiles",len(allprofiles))
print("length allprofiles fixincPA",len(allprofiles_fixincPA))
if (len(allprofiles) > 3):
    (rrs, v_Phi_prof, sv_Phi_prof, v_R_prof, sv_R_prof) = allprofiles # np.loadtxt(file_profile,unpack=True)
    DoAccr=True
    print("WARNING: found DoAcrr optimization but will plot vAccr only for fixincPA")
else:
    (rrs, v_Phi_prof, sv_Phi_prof) = np.loadtxt(file_profile,unpack=True)
    DoAccr=False

if (len(allprofiles_fixincPA) > 3):

    (rrs_fixincPA, v_Phi_prof_fixincPA, sv_Phi_prof_fixincPA, v_R_prof_fixincPA, sv_R_prof_fixincPA) = allprofiles_fixincPA 
    DoAccr_fixIncPA=True
    DoAccr=True
else:
    (rrs_fixincPA, v_Phi_prof_fixincPA, sv_Phi_prof_fixincPA) = allprofiles_fixincPA 
    DoAccr_fixIncPA=False
    DoAccr=False



# -----------------------------------------------------------
# nice fonts
# -----------------------------------------------------------
#matplotlib.rc('text', usetex=True) 
#matplotlib.rc('font', family='arial') 
matplotlib.rc('font', family='helvetica') 
matplotlib.rcParams.update({'font.size': 16})

if (DoAccr):
    f = plt.figure(figsize=(10, 15))
    axprofile=plt.subplot(313)
else:
    f = plt.figure(figsize=(10, 10))
    axprofile=plt.subplot(212)


(RunMCMC, proflist) = LogResults.load(workdir,FixPAInc=False)                

if RunMCMC:
    [r1s, r2s, rregions, incs, psis, PAs, allradsPA, allradsinc, allradspsi, PAs_MCMC, tanpsis_MCMC, incs_MCMC, allradsPAMCMC, allradsincMCMC,allradstanpsiMCMC] = proflist
else:
    [r1s, r2s, rregions, incs, psis, PAs, allradsPA, allradsinc, allradspsi] = proflist


a_min=np.min(r1s)
a_max=np.max(r2s)
print( "a_min ",a_min," a_max", a_max)

print( "psis",psis)
print( "allradspsi",allradspsi)
print( "incs",incs)
print( "incs MCMC",incs_MCMC)

psierrors=np.zeros((2,len(psis)))
if RunMCMC:
    for ir,atanpsi in enumerate(tanpsis_MCMC):
        psierrors[1,ir]=180.*(np.arctan(atanpsi[1]+atanpsi[0])-np.arctan(atanpsi[0]))/np.pi
        psierrors[0,ir]=180.*(np.arctan(atanpsi[0])-np.arctan(atanpsi[0]-atanpsi[2]))/np.pi


incerrors=np.zeros((2,len(incs)))
if RunMCMC:
    for ir,ainc in enumerate(incs_MCMC):
        print( "ainc",ainc)
        incerrors[1,ir]=1.*ainc[1]*180./np.pi
        incerrors[0,ir]=1.*ainc[2]*180./np.pi

PAerrors=np.zeros((2,len(PAs)))
if RunMCMC:
    for ir,aPA in enumerate(PAs_MCMC):
        PAerrors[1,ir]=1.*aPA[1]
        PAerrors[0,ir]=1.*aPA[2]



dPAs=PAs-allradsPA
dincs=incs-allradsinc
print( "dPAs",dPAs)
print( "allradsPA",allradsPA)
print( "PAs",PAs)
print( "dincs",dincs)

if (allradspsi < 0):
    dpsis=-psis+allradspsi
else:
    dpsis=psis-allradspsi


thislabel = r'$\mathrm{PA}-$%.1f$^\circ$'  %  allradsPA 
plt.plot(rregions,(PAs-allradsPA)  ,linewidth=1.,linestyle='solid',color='C0',label=thislabel,marker='o',markersize=4.)
print( "PA errors:",PAerrors)
axprofile.fill_between(rregions, (PAs-allradsPA)+PAerrors[1,:], (PAs-allradsPA)-PAerrors[0,:], lw=0.1,color='C0', alpha=0.2, interpolate=True) #, step='mid'


print( "PAerrors[1,:]",PAerrors[1,:])

thislabel = r'$i-%.1f^\circ$'  %  allradsinc 
plt.plot(rregions,(incs-allradsinc),linewidth=1.,linestyle='solid',color='C2',label=thislabel,marker='o',markersize=4.)
print( "inc errors:",incerrors)
axprofile.fill_between(rregions, (incs-allradsinc)+incerrors[1,:], (incs-allradsinc)-incerrors[0,:],lw=0.1,color='C2', alpha=0.2, interpolate=True) #, step='mid'

print(  "incerrors",incerrors[1,:])

if (allradspsi < 0):
    thislabel = r'$-\psi-%.1f^\circ$'  %  -allradspsi
    plt.plot(rregions,-(psis-allradspsi),linewidth=1.,linestyle='solid',color='C1',label=thislabel,marker='o',markersize=4.)
    axprofile.fill_between(rregions, -(psis-allradspsi)-psierrors[1,:], -(psis-allradspsi)+psierrors[0,:],lw=0.1,color='C1', alpha=0.2, interpolate=True) #, step='mid'
else:
    thislabel = r'$\psi-%.1f^\circ$'  %  allradspsi
    plt.plot(rregions,(psis-allradspsi),linewidth=1.,linestyle='solid',color='C1',label=thislabel,marker='o',markersize=4.)
    print( "psi errors:",psierrors)
    axprofile.fill_between(rregions, (psis-allradspsi)+psierrors[1,:], (psis-allradspsi)-psierrors[0,:],lw=0.1,color='C1', alpha=0.2, interpolate=True) #, step='mid'



sigma_PA=np.std(PAs)
sigma_inc=np.std(incs)
sigma_psi=np.std(psis)
print( "Psi= %.1f +- %.1f deg" % (allradspsi,sigma_psi))
print( "Inc= %.2f +- %.2f deg" % (allradsinc,sigma_inc))
print( "PA= %.1f +- %.1f deg" % (allradsPA,sigma_PA))


plt.ylabel(r'deg')
plt.xlabel(r'$r$ / arcsec')


print( "USING inclination ", allradsinc," for mass estimates")
cosi=np.fabs(np.cos(allradsinc*np.pi/180.))


#######################################################################
######################################################################


if (DoAccr):
    axprofile=plt.subplot(313)
else:
    axprofile=plt.subplot(212)


(RunMCMC, proflist) = LogResults.load(workdir_fixincPA,RunMCMC=RunMCMC,FixPAInc=True) 


if RunMCMC:
    [r1s,r2s, rregions_fixincPA, psis_fixincPA, allradspsi_fixincPA, tanpsis_fixincPA_MCMC]=proflist
else:
    [r1s,r2s, rregions_fixincPA, psis_fixincPA, allradspsi_fixincPA] = proflist
            
a_min=np.min(r1s)
a_max=np.max(r2s)
print( "a_min ",a_min," a_max", a_max)


print( "psis_fixincPA", psis_fixincPA)
print( "allradspsi_fixincPA",allradspsi_fixincPA)

psierrors_fixincPA=np.zeros((2,len(psis_fixincPA)))
if RunMCMC:
    print( "psis_fixincPA MCMC", tanpsis_fixincPA_MCMC)

    for ir,atanpsi in enumerate(tanpsis_fixincPA_MCMC):
        psierrors_fixincPA[1,ir]=180.*(np.arctan(atanpsi[1]+atanpsi[0])-np.arctan(atanpsi[0]))/np.pi
        psierrors_fixincPA[0,ir]=180.*(np.arctan(atanpsi[0])-np.arctan(atanpsi[0]-atanpsi[2]))/np.pi



print("psis_fixincPA",psis_fixincPA)
print("allradspsi_fixincPA",allradspsi_fixincPA)

if (allradspsi_fixincPA < 0):
    dpsis_fixincPA=-psis_fixincPA+allradspsi_fixincPA
else:
    dpsis_fixincPA=psis_fixincPA-allradspsi_fixincPA



if (allradspsi_fixincPA < 0):
    thislabel = r'$-\psi-%.1f^\circ$ fix $i,\mathrm{PA}$'  %  -allradspsi_fixincPA
    plt.plot(rregions_fixincPA,-(psis_fixincPA-allradspsi_fixincPA),linewidth=1.,linestyle='solid',color='grey',label=thislabel,marker='o',markersize=4.)
    print( "psi errors fixincPA:",psierrors_fixincPA)
    #plt.errorbar(rregions_fixincPA,(psis_fixincPA-allradspsi_fixincPA),yerr=psierrors_fixincPA,color='black')
    #print( "rregions_fixincPA",rregions_fixincPA)
    #print( "psis_fixincPA",psis_fixincPA.shape)
    #print( "psierrors_fixincPA", psierrors_fixincPA.shape)
    if RunMCMC:
        axprofile.fill_between(rregions_fixincPA, -(psis_fixincPA-allradspsi)-psierrors_fixincPA[1,:], -(psis_fixincPA-allradspsi)+psierrors_fixincPA[0,:],lw=0.1,color='grey', alpha=0.2, interpolate=True) #, step='mid'


    print( "-(psis_fixincPA-allradspsi)+psierrors_fixincPA[0,:]", )


    if RunMCMC:
        print( "dpsis_fixincPA+psierrors_fixincPA[1,:]",dpsis_fixincPA,psierrors_fixincPA[1,:], dpsis_fixincPA+psierrors_fixincPA[1,:])
        ymax=1.1*np.max(np.concatenate( (dPAs+PAerrors[1,:], dincs+incerrors[1,:], dpsis+psierrors[1,:], -(psis_fixincPA-allradspsi)+psierrors_fixincPA[1,:]   )))
        ymin=1.05*np.min(np.concatenate( (dPAs-PAerrors[0,:], dincs-incerrors[0,:], dpsis-psierrors[0,:], -(psis_fixincPA-allradspsi)-psierrors_fixincPA[0,:] )))
    else:
        ymax=1.1*np.max(np.concatenate( (dPAs, dincs, dpsis, -(psis_fixincPA-allradspsi)   )))
        ymin=1.05*np.min(np.concatenate( (dPAs, dincs, dpsis, -(psis_fixincPA-allradspsi) )))

    print( "ymax=",ymax)

else:
    thislabel = r'$\psi-%.1f^\circ$ fix $i,\mathrm{PA}$'  %  allradspsi
    plt.plot(rregions_fixincPA,(psis_fixincPA-allradspsi_fixincPA),linewidth=1.,linestyle='solid',color='grey',label=thislabel,marker='o',markersize=4.)
    print( "psi errors fixincPA:",psierrors_fixincPA)
    #plt.errorbar(rregions_fixincPA,(psis_fixincPA-allradspsi_fixincPA),yerr=psierrors_fixincPA,color='black')
    if RunMCMC:
        axprofile.fill_between(rregions_fixincPA, (psis_fixincPA-allradspsi)+psierrors_fixincPA[1,:], (psis_fixincPA-allradspsi)-psierrors_fixincPA[0,:],lw=0.1,color='grey', alpha=0.2, interpolate=True) #, step='mid'
    
    if RunMCMC:
        ymax=1.05*np.max(np.concatenate( (dPAs+PAerrors[1,:], dincs+incerrors[1,:], dpsis+psierrors[1,:], (psis_fixincPA-allradspsi)+psierrors_fixincPA[1,:]   )))
        ymin=1.05*np.min(np.concatenate( (dPAs-PAerrors[0,:], dincs-incerrors[0,:], dpsis-psierrors[0,:], (psis_fixincPA-allradspsi)-psierrors_fixincPA[0,:] )))
    else:
        ymax=1.05*np.max(np.concatenate( (dPAs, dincs, dpsis, (psis_fixincPA-allradspsi))))
        ymin=1.05*np.min(np.concatenate( (dPAs, dincs, dpsis, (psis_fixincPA-allradspsi))))

    print( "ymax=",ymax)

    #ymax=1.05*np.max(np.concatenate( (dPAs+PAerrors[1,:], dincs+incerrors[1,:], dpsis+psierrors[1,:], dpsis_fixincPA+psierrors_fixincPA[1,:] )))
    #ymin=1.05*np.min(np.concatenate( (dPAs-PAerrors[0,:], dincs-incerrors[0,:], dpsis-psierrors[0,:], dpsis_fixincPA-psierrors_fixincPA[0,:] )))

sigma_psi_fixincPA=np.std(psis_fixincPA)
print( "Psi= %.1f +- %.1f deg" % (allradspsi_fixincPA,sigma_psi_fixincPA))


plt.xlim(a_min,a_max)
plt.ylim(ymin, ymax)



#plt.legend()
#plt.legend(fontsize=16,loc='lower center')
plt.legend(fontsize=16)
plt.ylabel(r'deg')
plt.xlabel(r'$r$ / arcsec')

#plt.legend()
axprofile.tick_params(axis='both', length = 8,  direction='in', pad=10)
axprofile.tick_params(top='on',right='on', direction='in')
axprofile.tick_params(which='minor', top='on', length = 4, right='on', direction='in')
axprofile.xaxis.set_major_formatter(matplotlib.ticker.ScalarFormatter())
axprofile.xaxis.set_minor_formatter(matplotlib.ticker.ScalarFormatter())


print( "USING inclination ", allradsinc," for mass estimates")
#cosi=np.cos(40.06*np.pi/180.)
cosi=np.fabs(np.cos(allradsinc*np.pi/180.))



######################################################################
######################################################################
######################################################################



hhs=np.interp(rrs,rregions,np.tan(psis*np.pi/180.))
hhs_fixincPA=np.interp(rrs_fixincPA,rregions_fixincPA,np.tan(psis_fixincPA*np.pi/180.))


#print( "hhs",hhs)
correct4midplane = ( (1. + hhs**2)**(3./4.))
correct4midplane_fixincPA = ( (1. + hhs_fixincPA**2)**(3./4.))

v_Phi_prof_mid_fixincPA = v_Phi_prof_fixincPA * correct4midplane_fixincPA 

v_Phi_prof_mid = v_Phi_prof * correct4midplane

if (DoAccr):
    axprofile=plt.subplot(311)
else:
    axprofile=plt.subplot(211)


axprofile = plt.gca()

rmax=np.max(rrs)
 


axprofile.set_xlim(a_min,a_max)
maskrange=np.where( (rrs > a_min) & (rrs < a_max))


ymin=0.99*np.min((v_Phi_prof[maskrange]-sv_Phi_prof[maskrange]) * (np.sqrt(rrs[maskrange])/np.sqrt(a_max)))

dup = (v_Phi_prof_fixincPA[maskrange]+sv_Phi_prof_fixincPA[maskrange]) * (np.sqrt(rrs_fixincPA[maskrange])/np.sqrt(a_max))
#dup_corr = correct4midplane[maskrange]*(v_Phi_prof[maskrange]) * (np.sqrt(rrs[maskrange])/np.sqrt(a_max))
#dup_corr = correct4midplane_fixincPA[maskrange]*(v_Phi_prof_fixincPA[maskrange]) * (np.sqrt(rrs_fixincPA[maskrange])/np.sqrt(a_max))
dup_corr = v_Phi_prof_mid_fixincPA[maskrange] * (np.sqrt(rrs_fixincPA[maskrange])/np.sqrt(a_max))

ymax=1.005*np.max(np.concatenate((dup, dup_corr)))



axprofile.set_ylim(ymin,ymax)
plotmask = np.where( (rrs >= a_min) & (rrs <= a_max) )

axprofile.fill_between(rrs_fixincPA[plotmask], (v_Phi_prof_fixincPA[plotmask]+sv_Phi_prof_fixincPA[plotmask])*np.sqrt(rrs_fixincPA[plotmask])/np.sqrt(a_max), (v_Phi_prof_fixincPA[plotmask]-sv_Phi_prof_fixincPA[plotmask])*np.sqrt(rrs_fixincPA[plotmask])/np.sqrt(a_max), lw=0.1,color='grey', alpha=0.2, interpolate=True) #, step='mid'





DoStellarMass=True

if (DoStellarMass):

    # ----------------------------------------------------------------------
    # optimal mass
    # ----------------------------------------------------------------------
    from astropy import constants as const
    
    #print( "bmaj = ",bmaj,"\n";)
    Nind=2.*np.pi*rrs_fixincPA * cosi  /(bmaj) #cosi
    #print( "Nind=",Nind)
    #print( "sqrt(Nind)=",np.sqrt(Nind))
    dispv_Phi_prof=sv_Phi_prof_fixincPA.copy()
    sv_Phi_prof_mean=dispv_Phi_prof/np.sqrt(Nind)
    #sv_Phi_prof=dispv_Phi_prof#/np.sqrt(Nind)
    
    yr = 31558149.8
    RRs=rrs_fixincPA*distance*const.au.value

    nmesh=10
    rbins=np.arange(nmesh)*(a_max-a_min)/float(nmesh)+a_min
    Ms=np.zeros(nmesh)
    dr=rbins[1]-rbins[0]
        
    for irrs in np.arange(nmesh):
        #mask= ((rrs > 0.4) & (rrs < 0.6))
        mask= ((rrs > rbins[irrs]) & (rrs < rbins[irrs]+dr))
        
        # mask= ((rrs > 0.4) & (rrs < 0.8))
        v0=v_Phi_prof_fixincPA[np.where(mask)]
        sv0=sv_Phi_prof_mean[np.where(mask)]

        subRRs=RRs[np.where(mask)]
        
        Mstar_num =  (np.sum( v0 / (sv0**2 * np.sqrt(subRRs))))**2
        Mstar_denom = (np.sum(np.sqrt(1.-cosi**2)*1E-3*np.sqrt( const.G.value  * const.M_sun.value) / subRRs / sv0**2))**2
        Mstar = Mstar_num / Mstar_denom
        Ms[irrs] = Mstar
        print( "rrs ",rbins[irrs]," Mstar = ",Mstar )
        
        #Mstar = float("%.2f" % Mstar)
        

    #mask= ((rrs > 0.4) & (rrs < 0.6))
    mask= ((rrs > a_min) & (rrs < a_max))
    #mask= ((rrs > rbins[irrs]) & (rrs < rbins[irrs]+dr))
    
    # mask= ((rrs > 0.4) & (rrs < 0.8))
    v0=v_Phi_prof_fixincPA[np.where(mask)]
    sv0=sv_Phi_prof_mean[np.where(mask)]
    subRRs=RRs[np.where(mask)]
    
    Mstar_num =  (np.sum( v0 / (sv0**2 * np.sqrt(subRRs))))**2
    Mstar_denom = (np.sum(np.sqrt(1.-cosi**2)*1E-3*np.sqrt( const.G.value  * const.M_sun.value) / subRRs / sv0**2))**2
    Mstar = Mstar_num / Mstar_denom
    sMstar = (1./Mstar_denom) * np.sqrt( np.sum( 1./(sv0**2 * subRRs)))
    print( "Mstar = ",Mstar,"+-",sMstar,"Msun")
    
    print( "radial Mstar scatter: ",np.std(Ms),"Msun")

        
    vstar = np.sqrt(1.-cosi**2)*1E-3*np.sqrt( const.G.value  * Mstar * const.M_sun.value / RRs )
    vstar[0]=0.
    axprofile.plot(rrs_fixincPA[plotmask],vstar[plotmask]*np.sqrt(rrs_fixincPA[plotmask])/np.sqrt(a_max),color='grey',linewidth=2.0,alpha=1.0,linestyle='dashed',label=str("%.2f" % Mstar)+r'$\,M_\odot$')
        
    #Mstars=(1.7,1.8,1.9,2.0)
    # Mstars=(Mstar-0.1, Mstar+0.1)
    # for (iMstar,Mstar)in enumerate(Mstars):
    #    vstar = np.sqrt(1.-cosi**2)*1E-3*np.sqrt( const.G.value  * Mstar * const.M_sun.value / RRs )
    #    vstar[0]=0.
    #    axprofile.loglog(rrs[plotmask],vstar[plotmask],color='C'+str(iMstar+1),linewidth=0.6,linestyle='solid',label=str("%.1f" % Mstar)+r'$\,M_\odot$')
    
    #print( "min vstar",vstar.min())
    #print( "max vstar",vstar.max())



#axprofile.plot(rrs[plotmask],v_Phi_prof[plotmask]*np.sqrt(rrs)/np.sqrt(a_max),color='grey',linewidth=1.5,linestyle='solid',marker='o',markersize=3.,label='data surf.')
#axprofile.plot(rrs[plotmask],v_Phi_prof[plotmask]*np.sqrt(rrs)/np.sqrt(a_max),color='grey',linewidth=1.5,linestyle='solid',label='data')
axprofile.plot(rrs_fixincPA[plotmask],v_Phi_prof_fixincPA[plotmask]*np.sqrt(rrs_fixincPA[plotmask])/np.sqrt(a_max),color='grey',linewidth=1.5,linestyle='solid',label='data')


if (PlotContinuumGaps):
    rgaps=[0.4433,  0.8575, 1.3923, 2.3]
    for argap in rgaps:
        axprofile.plot([argap, argap],[ymin,ymax],color='black',linewidth=0.5,linestyle='dotted')
    




DoStellarMass_corrected=True

if (DoStellarMass_corrected):

    # ----------------------------------------------------------------------
    # optimal mass
    # ----------------------------------------------------------------------
    from astropy import constants as const
    
    bmaj=70E-3 # arcsec
    #print( "bmaj = ",bmaj,"\n";)
    Nind=2.*np.pi*rrs_fixincPA /(cosi * bmaj)
    #print( "Nind=",Nind)
    #print( "sqrt(Nind)=",np.sqrt(Nind))
    dispv_Phi_prof=sv_Phi_prof_fixincPA.copy()
    sv_Phi_prof_mean=dispv_Phi_prof/np.sqrt(Nind)
    #sv_Phi_prof=dispv_Phi_prof#/np.sqrt(Nind)
    
    yr = 31558149.8
    RRs=rrs_fixincPA*distance*const.au.value

    nmesh=10
    rbins=np.arange(nmesh)*(a_max-a_min)/float(nmesh)+a_min
    Ms=np.zeros(nmesh)
    dr=rbins[1]-rbins[0]
        
    for irrs in np.arange(nmesh):
        #mask= ((rrs > 0.4) & (rrs < 0.6))
        mask= ((rrs_fixincPA > rbins[irrs]) & (rrs_fixincPA < rbins[irrs]+dr))
        
        # mask= ((rrs > 0.4) & (rrs < 0.8))
        v0=v_Phi_prof_mid_fixincPA[np.where(mask)]
        sv0=sv_Phi_prof_mean[np.where(mask)]
        subRRs=RRs[np.where(mask)]
        
        Mstar_num =  (np.sum( v0 / (sv0**2 * np.sqrt(subRRs))))**2
        Mstar_denom = (np.sum(np.sqrt(1.-cosi**2)*1E-3*np.sqrt( const.G.value  * const.M_sun.value) / subRRs / sv0**2))**2
        Mstar = Mstar_num / Mstar_denom
        Ms[irrs] = Mstar
        print( "rrs ",rbins[irrs]," iMstar = ",Mstar )
        
        #Mstar = float("%.2f" % Mstar)
        

    #mask= ((rrs > 0.4) & (rrs < 0.6))
    mask= ((rrs_fixincPA > a_min) & (rrs_fixincPA < a_max))
    #mask= ((rrs > rbins[irrs]) & (rrs < rbins[irrs]+dr))
    
    # mask= ((rrs > 0.4) & (rrs < 0.8))
    v0=v_Phi_prof_mid_fixincPA[np.where(mask)]
    sv0=sv_Phi_prof_mean[np.where(mask)]
    subRRs=RRs[np.where(mask)]
    
    Mstar_num =  (np.sum( v0 / (sv0**2 * np.sqrt(subRRs))))**2
    Mstar_denom = (np.sum(np.sqrt(1.-cosi**2)*1E-3*np.sqrt( const.G.value  * const.M_sun.value) / subRRs / sv0**2))**2
    Mstar = Mstar_num / Mstar_denom
    sMstar = (1./Mstar_denom) * np.sqrt( np.sum( 1./(sv0**2 * subRRs)))
    print( "Mstar = ",Mstar,"+-",sMstar,"Msun")
    
    print( "radial Mstar scatter: ",np.std(Ms),"Msun")

        
    vstar = np.sqrt(1.-cosi**2)*1E-3*np.sqrt( const.G.value  * Mstar * const.M_sun.value / RRs )
    vstar[0]=0.
    axprofile.plot(rrs[plotmask],vstar[plotmask]*np.sqrt(rrs[plotmask])/np.sqrt(a_max),color='cornflowerblue',linewidth=2.0,alpha=1.0,linestyle='dashed',label=str("%.2f" % Mstar)+r'$\,M_\odot$')
        
    # #Mstars=(1.7,1.8,1.9,2.0)
    # Mstars=(Mstar-0.1, Mstar+0.1)
    # for (iMstar,Mstar)in enumerate(Mstars):
    #     vstar = np.sqrt(1.-cosi**2)*1E-3*np.sqrt( const.G.value  * Mstar * const.M_sun.value / RRs )
    #     vstar[0]=0.
    #     axprofile.loglog(rrs[plotmask],vstar[plotmask],color='C'+str(iMstar+1),linewidth=0.6,linestyle='solid',label=str("%.1f" % Mstar)+r'$\,M_\odot$')
    



print( "ymax v_Phi_prof_mid_fixincPA", np.max(v_Phi_prof_mid_fixincPA[plotmask]*np.sqrt(rrs[plotmask])/np.sqrt(a_max)))
print( "ymax v_Phi_prof_mid_fixincPA", np.max(v_Phi_prof_mid_fixincPA[plotmask]*np.sqrt(rrs_fixincPA[plotmask])/np.sqrt(a_max)))






#axprofile.plot(rrs[plotmask],v_Phi_prof_mid[plotmask]*np.sqrt(rrs)/np.sqrt(a_max),color='blue',linewidth=1.5,linestyle='solid',marker='o',markersize=3.,label='data mid.')
axprofile.plot(rrs_fixincPA[plotmask],v_Phi_prof_mid_fixincPA[plotmask]*np.sqrt(rrs[plotmask])/np.sqrt(a_max),color='cornflowerblue',linewidth=1.5,linestyle='solid',label='data mid.')




axprofile.legend(fontsize=16)

#if DoMidplaneCorr:
#    axprofile.set_ylabel(r'midplane  $\tilde{v}_{\phi}(r)$ / km s$^{-1}$')
#else:


axprofile.set_ylabel(r'$\sqrt{r/'+str(a_max)+'} \\times \\tilde{v}_{\phi}(r)$ / km s$^{-1}$')



#axprofile.set_xlabel(r'$r$ / arcsec')



axprofile.tick_params(axis='both', length = 8,  direction='in', pad=10)
axprofile.tick_params(top='on',right='on', direction='in')
axprofile.tick_params(which='minor', top='on', length = 4, right='on', direction='in')


#from matplotlib.ticker import ScalarFormatter
#axprofile.xaxis.set_major_formatter(matplotlib.ticker.ScalarFormatter())
#axprofile.xaxis.set_minor_formatter(matplotlib.ticker.ScalarFormatter())
#axprofile.yaxis.set_major_formatter(matplotlib.ticker.ScalarFormatter())
#axprofile.yaxis.set_minor_formatter(matplotlib.ticker.ScalarFormatter())


plt.setp(axprofile.get_xticklabels(),visible=False) #, fontsize=6)



if (DoAccr):
    axprofile=plt.subplot(312)

    ymin=1.01*np.min((v_R_prof_fixincPA[maskrange] - sv_R_prof_fixincPA[maskrange]) * (np.sqrt(rrs_fixincPA[maskrange])/np.sqrt(a_max)))
    ymax=1.01*np.max((v_R_prof_fixincPA[maskrange] + sv_R_prof_fixincPA[maskrange]) * (np.sqrt(rrs_fixincPA[maskrange])/np.sqrt(a_max)))



    axprofile.plot(rrs_fixincPA[plotmask],v_R_prof_fixincPA[plotmask]*np.sqrt(rrs_fixincPA[plotmask])/np.sqrt(a_max),color='red',linewidth=1.5,linestyle='solid',label=r'data $v_R$')
    axprofile.set_ylabel(r'$\sqrt{r/'+str(a_max)+'} \\times \\tilde{v}_{R}(r)$ / km s$^{-1}$')

    print( "wcols v_R")
    print((np.array(zip(rrs_fixincPA[plotmask],v_R_prof_fixincPA[plotmask]+sv_R_prof_fixincPA[plotmask]))))

    axprofile.fill_between(rrs_fixincPA[plotmask], (v_R_prof_fixincPA[plotmask]+sv_R_prof_fixincPA[plotmask])*np.sqrt(rrs_fixincPA[plotmask])/np.sqrt(a_max), (v_R_prof_fixincPA[plotmask]-sv_R_prof_fixincPA[plotmask])*np.sqrt(rrs_fixincPA[plotmask])/np.sqrt(a_max), lw=0.1,color='red', alpha=0.2, interpolate=True) #, step='mid'

    plt.xlim(a_min,a_max)
    plt.ylim(ymin,ymax)
    axprofile.legend(fontsize=16)

    axprofile.tick_params(axis='both', length = 8,  direction='in', pad=10)
    axprofile.tick_params(top='on',right='on', direction='in')
    axprofile.tick_params(which='minor', top='on', length = 4, right='on', direction='in')
    
    plt.setp(axprofile.get_xticklabels(),visible=False) #, fontsize=6)
    
    if (PlotContinuumGaps):
        rgaps=[0.4433,  0.8575, 1.3923, 2.3]
        for argap in rgaps:
            axprofile.plot([argap, argap],[ymin,ymax],color='black',linewidth=0.5,linestyle='dotted')




plt.subplots_adjust(hspace=0.)

print( fileout_fig)
f.savefig(fileout_fig, bbox_inches='tight')


