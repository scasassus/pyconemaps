import sys
import numpy as np
import re

import os
HOME=os.environ.get('HOME')
#include_path='/Users/simon/common/python/include/'
include_path=HOME+'/common/python/conemaps-git/'
sys.path.append(include_path)
import MasterDConeMaps

######################################################################


S=MasterDConeMaps.Setup(
    filename_source='./dgaussminuit_nobaseline/im_gmom_8.fits',
    filename_errormap='./dgaussminuit_nobaseline/im_gmom_8_e.fits',

    # workdir='work_gmom_8_werrmap/',  # with trailing back slash
    # workdir='work_gmom_8_werrmap_wAccrFixPAinc_pf2/',  # with trailing back slash
    # workdir='work_gmom_8_werrmap_wAccrFixPAinc/',  # with trailing back slash
    # workdir='work_gmom_8_werrmap_wAccrFixPAinc_XCheck/',  # with trailing back slash
    workdir='work_gmom_8_werrmap/',  # with trailing back slash
    # workdir='work_gmom_8_werrmap_wAccrFixPAinc/',  # with trailing back slash

    DoErrorMap=True,
    typicalerror=0.1, #  km/s

    ComputeSystVelo=True,  # best run this only once, then pass value in vsyst
    vsyst= 5.76860412669,

    fieldscale=1.,
    pixscale_factor=1.0,   #6.0
    unitscale=1.,

    PA=312.379492,
    inc=2.326563,
    tanpsi=-0.213911,

    rangePA=40.,
    rangeinc=15.*np.pi/180.,
    rangetanpsi=0.8,

    a_min=0.35,
    a_max=4.0,

    DoRegions=True,
    a_min_regions=0.35,
    a_max_regions=4.0,
    n_abins=10, #minimum 3 for overlap

    DoAccr=False,
    DoAccr_fixPAinc=True,

    ClearWorkDir=True,
    DoExec=True,  # Execute the full optimization
    DoFixOrient=True, # Execute the restricted optimization, with fixed orientation

    DumpAllFitsFiles=False,

    x_center=0.,
    y_center=0.,

    #python ~/common/python/simon_examplescripts/beam.py    /Users/simon/common/ppdisks/HD163296/kine/data/HD163296_CO.fits
    #0.104 x 0.095 / -80.2 
    bmaj=0.104, # arcsec
    bmin=0.095, # arcsec

    DoConjGrad=True,
    DoMinuit=False,  # BROKEN 

    RunMCMC=False,
    RecoverMCMC=False, # RunMCMC
    n_cores_MCMC=5, #30
    Nit=80,
    burn_in=50,
    nwalkers= 20)

S.domain=( ('PA',(S.PA-S.rangePA/2.,S.PA+S.rangePA/2.)), ('inc',(S.inc-S.rangeinc/2.,S.inc+S.rangeinc/2.)),('tanpsi',(S.tanpsi-S.rangetanpsi/2.,S.tanpsi+S.rangetanpsi/2.)))


if S.DoExec:
    S.Run()

if S.DoFixOrient:
    S.RunFixOrient()

