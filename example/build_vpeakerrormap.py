import os
import re
from astropy.io import fits 
import scipy
import scipy.signal

import matplotlib as mpl
# mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from pylab import *
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
from  scipy.signal import medfilt2d

include_path='/Users/simon/common/python/include/'
sys.path.append(include_path)

import Vtools


                
View=False

def exec_medfilt(im0,ksize=3,nsigma=20.,viewtmp=False, filter_againstnoise=False):
        medim=medfilt2d(im0,kernel_size=ksize)
        diffim=im0-medim
        typicalerror=np.sqrt(np.median(diffim**2))
        print("typicalerror in medfilt",typicalerror)
        if filter_againstnoise:
                mask=np.where(np.fabs(diffim) < nsigma * typicalerror)
        else:
                mask=np.where(np.fabs(diffim) < nsigma * np.fabs(medim))

        immask=np.zeros(im0.shape)
        immask[mask]=1
        if viewtmp:
                print("medim")
                Vtools.View(medim)
                print("difference")
                Vtools.View(np.fabs(diffim)/typicalerror)
                print("mask")
                Vtools.View(immask,cmap='bone')


        return immask

def exec_filttypical(im0,nsigmaup=2.):
	typicalerror=np.median(im0)
	mask=np.where( (im0 < nsigmaup*typicalerror))
	immask=np.zeros(im0.shape)
	immask[mask]=1
	return immask,typicalerror

def exec_filtlow(im0,nsigmalow=100.):
	typicalerror=np.median(im0)
	mask=np.where( im0 < typicalerror/nsigmalow)
	im0[mask]=typicalerror
	return im0

def exec_errormap(workdir,ng=2):
        print( "workdir:",workdir)
        if (ng==2):
                filename_vpeak=workdir+'im_gmom_8.fits'
        else:
                filename_vpeak=workdir+'im_g_v0.fits'
                
        filename_fiterrormap=workdir+'fiterrormap.fits'
        filename_g_a_e=workdir+'im_g_a_e.fits'
        filename_g_v0_e=workdir+'im_g_v0_e.fits'
        filename_g_sigma_e=workdir+'im_g_sigma_e.fits'
        badpixelvalue=1E20 # np.inf


        f = fits.open(filename_fiterrormap)
        im_fiterrormap = f[0].data
        hdr_fiterrormap = f[0].header

        immask,typicalerror = exec_filttypical(im_fiterrormap,nsigmaup=5.)
        print("typical rms error in specific intensity (fiterrormap):",typicalerror)

        if View:
                print("viewing fiterrormap immask")
                Vtools.View(immask)


        f = fits.open(filename_g_v0_e)
        im_verrormap = f[0].data
        hdr_verrormap= f[0].header
        #immask_verrormap  = exec_medfilt(im_verrormap,ksize=101,nsigma=5.)
        immask_verrormap, typicalerror_v0  = exec_filttypical(im_verrormap,nsigmaup=8.)
        if View:
                print("viewing verrormap immask")
                Vtools.View(immask_verrormap)


        immask  = immask * immask_verrormap

        print(workdir+'im_verror_mask.fits')
        fits.writeto(workdir+'im_verror_mask.fits',immask_verrormap,hdr_fiterrormap,overwrite=True)

        #f = fits.open(filename_g_a_e)
        #im_g_a_e = f[0].data
        ##immask  = immask * exec_medfilt(im_g_a_e)
        #immask  = immask * exec_filttypical(im_g_a_e,nsigma=20.)[0]
        #
        #f = fits.open(filename_g_sigma_e)
        #im_g_sigma_e = f[0].data
        ##immask  = immask * exec_medfilt(im_g_sigma_e)
        #immask  = immask * exec_filttypical(im_g_sigma_e,nsigma=20.)[0]

        FilterVpeak=True
        if FilterVpeak:
                f = fits.open(filename_vpeak)
                im_vpeak = f[0].data
                immask_vpeak  = exec_medfilt(im_vpeak,ksize=5,nsigma=100.0,viewtmp=True,filter_againstnoise=True)
                print(workdir+'im_vpeak_mask.fits')
                fits.writeto(workdir+'im_vpeak_mask.fits',immask_vpeak,hdr_fiterrormap,overwrite=True)
                immask  = immask * immask_vpeak
                if View:
                        print("viewing vpeak mask")
                        Vtools.View(immask_vpeak,cmap='bone')

        if View:
                print("viewing master immask")
                Vtools.View(immask)

        im_verrormap=exec_filtlow(im_verrormap)
        im_verrormap[np.where(immask == 0)] = badpixelvalue

        print(workdir+'master_mask.fits')
        fits.writeto(workdir+'master_mask.fits',immask,hdr_fiterrormap,overwrite=True)
        if (ng==1):
                print(workdir+'im_g_v0_errormap.fits')
                fits.writeto(workdir+'im_g_v0_errormap.fits',im_verrormap,hdr_verrormap,overwrite=True)
        else:
                print(workdir+'im_gmom_8_e.fits')
                fits.writeto(workdir+'im_gmom_8_e.fits',im_verrormap,hdr_verrormap,overwrite=True)
                

        return



#workdir='dgaussminuit_CommonSigma/'
#workdir='dgaussminuit_narrow/'
workdir='/Users/simon/common/ppdisks/HD135344B/DMoments/12CO_sgaussmoments_constub_z/' # im_g_v0.fits 
#workdir='dgaussminuit_nobaseline/'
workdir='/Users/simon/common/ppdisks/HD135344B/DMoments/12CO_sgaussminuit_contsub_guvmem_lS0.003_lL0.0_smooth/' # im_g_v0.fits 
exec_errormap(workdir,ng=1)

