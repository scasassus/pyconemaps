import sys
import numpy as np
import os
import os.path
import  scipy as sp
from scipy import ndimage
from astropy.io import fits as pf
import re
include_path='/Users/simon/common/python/include/'
sys.path.append(include_path)
from Resamp import *
from copy import deepcopy
from astropy.wcs import WCS
from pylab import *

#import pyfits
#import matplotlib.pyplot as plt
#import re
#import scipy as S
#import scipy.ndimage
#from scipy import ndimage





def cartesian2polar(outcoords, inputshape, origin, fieldscale=1.):
    """Coordinate transform for converting a polar array to Cartesian coordinates. 
    inputshape is a tuple containing the shape of the polar array. origin is a
    tuple containing the x and y indices of where the origin should be in the
    output array."""

    rindex, thetaindex = outcoords
    x0, y0 = origin

    theta = thetaindex * 2 * np.pi / (inputshape[0]-1)
    #theta = 2. * np.pi - theta
    
    y = rindex*np.cos(theta)/fieldscale
    x = rindex*np.sin(theta)/fieldscale

    #print( "r",rindex,"theta",theta,"x",x,"y",y)
    
    ix = -x + x0
    iy = y +  y0

    return (iy,ix)


def polar2cartesian(outcoords, inputshape, origin, fieldscale=1.):
    yindex, xindex = outcoords
    x0, y0 = origin
#    theta0, r0 = origin
    nx=inputshape[0]-1
    ny=inputshape[1]-1
#    x0=((nx+1)/2)-1
#    y0=((ny+1)/2)-1
    x = -float(xindex) + x0
    y = float(yindex) -  y0
    
    #    theta = np.arctan2(-y, -x) + np.pi
    
    theta = np.arctan2(x, y) 
    if (theta < 0):
        theta = theta + 2.*np.pi
    #    theta = np.arctan2(-y, x) + np.pi
     
    thetaindex = (theta * nx / (2. * np.pi))
    #theta_index = np.round((theta + np.pi) * (inputshape[1]-1) / (2 * np.pi))
    
    #    thetaindex = np.arctan2(yindex-y0,xindex-x0) * nx / (2. * np.pi)
    rindex = fieldscale * np.sqrt(x**2 + y**2)
    
#    itheta = thetaindex + theta0
#    ir = rindex + r0

    #print( xindex, yindex, " x ",x," y ", y," theta ",theta)
    
    return (rindex,thetaindex)

#
#def polar2cartesian(outcoords, inputshape, origin):
#
#    xindex, yindex = outcoords
#    x0, y0 = origin
#    x = xindex - x0
#    y = yindex - y0
#
#    r = np.sqrt(x**2 + y**2)
#    theta = np.arctan2(y, x)
#
#    theta_index = np.round((theta + np.pi) * (inputshape[1]-1) / (2 * np.pi))
#    
#    return (r,theta_index)
#


#def polar2cartesian(outcoords, inputshape, origin):
#    """Coordinate transform for converting a polar array to Cartesian coordinates. 
#    inputshape is a tuple containing the shape of the polar array. origin is a
#    tuple containing the x and y indices of where the origin should be in the
#    output array."""
#
#    xindex, yindex = outcoords
#    x0, y0 = origin
#    x = xindex - x0
#    y = yindex - y0
#
#    r = np.sqrt(x**2 + y**2)
#    theta = np.arctan2(y, x)
#
#    theta_index = np.round((theta + np.pi) * inputshape[1] / (2 * np.pi))
#
#    return (r,theta_index)
#





def exec_polar_expansions(filename_source,workdir,PA,cosi,RA=False,DEC=False,alpha_min=False,Delta_min=False,ErrorMap=False,XCheckInv=False,DoRadialProfile=True,ProfileExtractRadius=-1,DoAzimuthalProfile=False,PlotRadialProfile=True,a_min=-1,a_max=-1,OptimOrient=False,vsyst=0.,ComputeSystVelo=True,LegBkgd=-1,ExpectedError=False,InjectNoise=False, DumpAllFitsFiles=False):

    fieldscale=2. # shrink radial field of view of polar maps by this factor

    inbasename=os.path.basename(filename_source)
    filename_fullim=re.sub('.fits', '_fullim.fits', inbasename)
    filename_fullim=workdir+filename_fullim

    fileout_centered=re.sub('fullim.fits', 'centered.fits', filename_fullim)


    
    DoErrorMap=False
    if (isinstance(ErrorMap,basestring)):
        inbasenameerr=os.path.basename(ErrorMap)
        filename_fullimerr=re.sub('.fits', '_fullim.fits', inbasenameerr)
        filename_fullimerr=workdir+filename_fullimerr
        fileout_centeredw=re.sub('fullim.fits', 'wcentered.fits', filename_fullimerr)
        DoErrorMap=True


    typicalerror=ExpectedError
    #print( "typicalerr= ",typicalerr)


    if (not OptimOrient): 
        os.system("rm -rf  "+workdir)
        os.system("mkdir "+workdir)    

    if ((OptimOrient) and (os.path.isfile(fileout_centered))):
        #print( "LOADING fileout_centered")
        hdu = pf.open(fileout_centered)
        resamp=hdu[0].data
        hdr2=hdu[0].header
        nx=hdr2['NAXIS1']
        ny=hdr2['NAXIS2']

        if (DoErrorMap):
            hduw = pf.open(fileout_centeredw)
            resampw=hduw[0].data

    else:
        print( "BUILDING WORKDIR AND GENERATING CENTERED IMAGE")
        os.system("rm -rf  "+workdir)
        os.system("mkdir "+workdir)    
        hdu0 = pf.open(filename_source)
        hdr0 = hdu0[0].header
        if (hdr0['NAXIS'] > 2):
            hdu=cube2im(filename_source,filename_fullim)
            im1=hdu.data
            hdr1=hdu.header
        else:
            os.system("rsync -va "+filename_source+" "+filename_fullim)
            hdu = pf.open(filename_fullim)
            im1=hdu[0].data
            hdr1=hdu[0].header


        if (DoErrorMap):
    
            hduerr0 = pf.open(ErrorMap)
            hdrerr0 = hduerr0[0].header
            if (hdrerr0['NAXIS'] > 2):
                hduerr=cube2im(ErrorMap,filename_fullimerr)
                imerr1=hduerr.data
                hdrerr1=hduerr.header
            else:
                os.system("rsync -va "+ErrorMap+" "+filename_fullimerr)
                hduerr = pf.open(filename_fullimerr)
                imerr1=hduerr[0].data
                hdrerr1=hduerr[0].header
            

        if (not (isinstance(Delta_min,bool))):
            if (not isinstance(RA,float)):
                RA=hdr1['CRVAL1']
                DEC=hdr1['CRVAL2']
                print( "using center of coords CRVAL1 CRVAL2")

            
            RA=RA+(np.sin(alpha_min*np.pi/180.)*Delta_min/3600.)/np.cos(DEC*np.pi/180.)
            print( "RA =", RA)
            DEC=DEC+np.cos(alpha_min*np.pi/180.)*Delta_min/3600.
            print( "DEC =",DEC)

        elif (not isinstance(RA,float)):
            sys.exit("must provide a center")

        nx=int(hdr1['NAXIS1']/2.5)
        ny=nx
        if ( (nx % 2) == 0):
            nx=nx+1
            ny=ny+1

        hdr2 = deepcopy(hdr1)

        hdr2['NAXIS1']=nx
        hdr2['NAXIS2']=ny
        hdr2['CRPIX1']=(nx+1)/2
        hdr2['CRPIX2']=(ny+1)/2
        hdr2['CRVAL1']=RA
        hdr2['CRVAL2']=DEC

        
        resamp=gridding(filename_fullim,hdr2, fullWCS=False)

        if (InjectNoise):
            resamp=resamp + np.random.normal(loc=0.0, scale=typicalerror, size=resamp.shape)
        
        fileout_centered=re.sub('fullim.fits', 'centered.fits', filename_fullim)
        pf.writeto(fileout_centered,resamp, hdr2, overwrite=True)

                
        if (DoErrorMap):
            resamperr=gridding(filename_fullimerr,hdr2, fullWCS=False)
            if (ExpectedError==False):
                typicalerr=np.median(resamperr)

            print( "typicalerr= ",typicalerr)

            resampw=(1./(resamperr**2))

            resampw[np.where(resamperr < 0.2*typicalerr)] = 0.
            resampw[np.where(resamperr > 3.*typicalerr)] = 0.

            fresampw=sp.ndimage.filters.median_filter(resampw, size=4)

            resampw[np.where( np.fabs(fresampw - resampw) > typicalerr)] = 0.            

            #print( "min resampw",resampw.min())

            fileout_centeredw=re.sub('fullim.fits', 'wcentered.fits', filename_fullimerr)
            pf.writeto(fileout_centeredw,resampw, hdr2, overwrite=True)


    
    
    #rotangel= PA - 180.
    rotangle= PA - 180.
    #    rotangle= PA
    im1rot = ndimage.rotate(resamp, rotangle, reshape=False)
    fileout_rotated=re.sub('fullim.fits', 'rotated.fits', filename_fullim)
    pf.writeto(fileout_rotated,im1rot, hdr2, overwrite=True)

    if (DoErrorMap):
        im1rotw = ndimage.rotate(resampw, rotangle, reshape=False)
        #print( "min im1rotw",im1rotw.min())
        im1rotw[np.where(im1rotw < 0.2*typicalerr)] = 0.

        fileout_rotatedw=re.sub('fullim.fits', 'wrotated.fits', filename_fullimerr)
        pf.writeto(fileout_rotatedw,im1rotw, hdr2, overwrite=True)

    

    hdr3 = deepcopy(hdr2)
    hdr3['CDELT1']=hdr3['CDELT1']*cosi
    fileout_stretched=re.sub('fullim.fits', 'stretched.fits', filename_fullim)
    im3=np.double(gridding(fileout_rotated,hdr3))
    pf.writeto(fileout_stretched,im3, hdr2, overwrite=True)


    if (DoErrorMap):
        fileout_stretchedw=re.sub('fullim.fits', 'wstretched.fits', filename_fullimerr)
        im3w=np.double(gridding(fileout_rotatedw,hdr3))
        pf.writeto(fileout_stretchedw,im3w, hdr2, overwrite=True)
        #print( "min im3w",im3w.min())
        im3w[np.where(im3w < 0.2*typicalerr)] = 0.


    im_polar = sp.ndimage.geometric_transform(im3,cartesian2polar, 
                                              order=1,
                                              output_shape = (im3.shape[0], im3.shape[1]),
                                              extra_keywords = {'inputshape':im3.shape,'fieldscale':fieldscale,
                                                                'origin':(((nx+1)/2)-1,((ny+1)/2)-1)}) 
    
    nphis,nrs=im_polar.shape
    
    hdupolar = pf.PrimaryHDU()
    hdupolar.data = im_polar
    hdrpolar=hdupolar.header
    hdrpolar['CRPIX1']=1
    hdrpolar['CRVAL1']=0.
    hdrpolar['CDELT1']=2. * np.pi / nphis
    hdrpolar['CRPIX2']=1
    hdrpolar['CRVAL2']=0.
    hdrpolar['CDELT2']=(hdr3['CDELT2'] / fieldscale)
    hdupolar.header = hdrpolar
    
    fileout_polar=re.sub('fullim.fits', 'polar.fits', filename_fullim)
    if (DumpAllFitsFiles):
        hdupolar.writeto(fileout_polar, overwrite=True)




    if (DoErrorMap):
        im_polarw = sp.ndimage.geometric_transform(im3w,cartesian2polar, 
                                                  order=1,
                                                  output_shape = (im3w.shape[0], im3w.shape[1]),
                                                  extra_keywords = {'inputshape':im3w.shape,'fieldscale':fieldscale,
                                                                'origin':(((nx+1)/2)-1,((ny+1)/2)-1)}) 
    

        im_polarw[np.where(im_polarw < 0.2*typicalerr)] = 0.

        nphis,nrs=im_polarw.shape
        
        hdupolarw = pf.PrimaryHDU()
        hdupolarw.data = im_polarw
        hdrpolarw=hdupolarw.header
        hdrpolarw['CRPIX1']=1
        hdrpolarw['CRVAL1']=0.
        hdrpolarw['CDELT1']=2. * np.pi / nphis
        hdrpolarw['CRPIX2']=1
        hdrpolarw['CRVAL2']=0.
        hdrpolarw['CDELT2']=(hdr3['CDELT2'] / fieldscale)
        hdupolarw.header = hdrpolar
        
        fileout_polarw=re.sub('fullim.fits', 'wpolar.fits', filename_fullimerr)
        if (DumpAllFitsFiles):
            hdupolarw.writeto(fileout_polarw, overwrite=True)
            

    ######################################################################
    # profiles




    if (DoRadialProfile):


        if (DoErrorMap):
            weights=im_polarw.copy()

            #if (not OptimOrient):
            #fileout_polarweights=re.sub('fullim.fits', 'polarweights.fits', filename_fullimerr)
            #if (not OptimOrient):
            #pf.writeto(fileout_polarweights,weights, hdrpolarerr, overwrite=True)

        #    Iprof = np.sum(im_polar*weights,axis=1)/np.sum(weights,axis=1)
        #else:
        #    Iprof = np.average(im_polar,axis=1)


        #Iprof_b = Iprof.reshape(Iprof.shape[0],1)

            
        im_polar_av = np.copy(im_polar)
        #im_polar_av[:,:] = Iprof_b

        rrs =  3600.*(np.arange(hdrpolar['NAXIS2'])-hdrpolar['CRPIX2']+1.)*hdrpolar['CDELT2']+hdrpolar['CRVAL2']
        phis =  (180./np.pi)*((np.arange(hdrpolar['NAXIS1'])-hdrpolar['CRPIX1']+1.)*hdrpolar['CDELT1']+hdrpolar['CRVAL1'])
        phis_rad =  np.double((np.arange(hdrpolar['NAXIS1'])-hdrpolar['CRPIX1']+1.)*hdrpolar['CDELT1']+hdrpolar['CRVAL1'])
        KepAmps= np.zeros(len(rrs))

        
        #errKepAmps= np.zeros(len(rrs))

        vsysts=np.zeros(hdrpolar['NAXIS2'])


        if (a_min > 0):
            ia_min = np.argmin(np.abs(rrs - a_min))
            #print( "a_min",a_min," ia_min",ia_min,len(rrs))

        if (a_max > 0):
            ia_max = np.argmin(np.abs(rrs - a_max))
            #print( "a_max",a_max," ia_max",ia_max,len(rrs))

                

        
        if (ComputeSystVelo):
            if (DoErrorMap):
                for irrs in xrange(len(rrs)):
                    v0_vec=im_polar[irrs,:]
                    w_vec=im_polarw[irrs,:]
                    #vsysts[irrs]=(np.max(v0_vec)+np.min(v0_vec))/2.
                    vsysts[irrs]=np.sum(w_vec*v0_vec)/np.sum(w_vec) #(np.max(v0_vec)+np.min(v0_vec))/2.
            else:
                for irrs in xrange(len(rrs)):
                    v0_vec=im_polar[irrs,:]
                    vsysts[irrs]=np.average(v0_vec)



                
            vsyst=np.asscalar(np.median(vsysts[ia_min:ia_max]))
            
        if (not OptimOrient):
            print( "vsyst = ",vsyst)

        
        for irrs in xrange(len(rrs)):
            if (DoErrorMap):
                v0_vec=im_polar[irrs,:]-vsyst
                w_vec=weights[irrs,:]
                KepAmp = np.sum(w_vec* v0_vec * np.cos(phis_rad)) / np.sum(w_vec * (np.cos(phis_rad))**2)
            else:
                v0_vec=im_polar[irrs,:]-vsyst
                KepAmp = np.sum(v0_vec * np.cos(phis_rad)) / np.sum((np.cos(phis_rad))**2)

            
            KepAmps[irrs] = KepAmp
            v0_vec_av = KepAmp*np.cos(phis_rad)
            im_polar_av[irrs,:] = v0_vec_av+vsyst
            #print((np.array(zip(phis_rad,v0_vec,v0_vec_av))))
            #print( "KepAmp",KepAmp)
            #sys.exit()

            
        #np.savetxt(workdir+'profile_Kep.dat',zip(rrs,np.fabs(KepAmps)),delimiter='  ')
                    
            

        Iprof = np.fabs(KepAmps) # np.average(im_polar_av,axis=1)
        Iprof=sp.nan_to_num(Iprof)

        if (DoErrorMap):
            sIprof = np.sqrt(np.sum(weights*(im_polar-im_polar_av)**2,axis=1)/np.sum(weights,axis=1))
            #print( "min weights",np.min(weights))
            sIprof=sp.nan_to_num(sIprof)

            
            zeimage=weights*(im_polar-im_polar_av)**2
            #print( np.max(zeimage[ia_min:ia_max]))
            #print( np.min(zeima[ia_min:ia_max]))
            deltaChi2 =  np.sum(zeimage,axis=1)
            #print( "deltaChi2 zevec",deltaChi2[ia_min:ia_max])
        else:
            sIprof = np.std(im_polar-im_polar_av,axis=1)
            deltaChi2 = sIprof**2/typicalerror**2

        #np.savetxt('test.out', (rrs,Iprof))  
        
        #bmaj=hdr2['BMAJ']
        #print( "bmaj = ",bmaj,"\n";)
        #Nind=2.*np.pi*rrs /(cosi * bmaj*3600.)
        #dispIprof=sIprof.copy()
        #sIprof=sIprof/np.sqrt(Nind)
        
        save_prof = np.zeros((hdrpolar['NAXIS2'],3))
        #print( save_prof.shape)
        save_prof[:,0] = rrs
        save_prof[:,1] = Iprof
        save_prof[:,2] = sIprof
        fileout_radialprofile=re.sub('fullim.fits', 'radial_profile.dat', filename_fullim)
        np.savetxt(fileout_radialprofile, save_prof)   # x,y,z equal sized 1D arrays


        

        ######################################################################
        # Polar average
        
    
        
        
        fileout_polar_av=re.sub('fullim.fits', 'polar_av.fits', filename_fullim)
        hdupolar.data = im_polar_av
        if (DumpAllFitsFiles):
            hdupolar.writeto(fileout_polar_av, overwrite=True)
        
            
            ######################################################################
            #Diff in polar maps
            im_polar_diff = im_polar - im_polar_av
            fileout_polar_diff=re.sub('fullim.fits', 'polar_diff.fits', filename_fullim)
            hdupolar.data = im_polar_diff
            hdupolar.writeto(fileout_polar_diff, overwrite=True)

            #if (nLegBkgd > 0):
            #from  SubLegBckgd  import *
            #hdupolar_sub = exec_LegBkgd(hdupolar,nLegBkg)
            #fileout_polar_sub=re.sub('fullim.fits', 'polar_diff_sub.fits', filename_fullim)
            #hdupolar_sub.writeto(fileout_polar_sub, overwrite=True)
                
                
    
    
            ######################################################################
            # AZIM IM 
        
            
            imazim = sp.ndimage.geometric_transform(im_polar_av,polar2cartesian,
                                                    order=1,
                                                    output_shape = (im_polar.shape[0], im_polar.shape[1]),
                                                    extra_keywords = {'inputshape':im_polar.shape,'fieldscale':fieldscale,
                                                                      'origin':(((nx+1)/2)-1,((ny+1)/2)-1)}) 
        
    
            fileout_stretched_av=re.sub('fullim.fits', 'stretched_av.fits', filename_fullim)
            pf.writeto(fileout_stretched_av,imazim, hdr2, overwrite=True)


        
            ######################################################################
            # back to sky - project 
            
            
            hdr3 = deepcopy(hdr2)
            hdr3['CDELT1']=hdr3['CDELT1']/cosi
            
            fileout_proj=re.sub('fullim.fits', 'azim_av_proj.fits', filename_fullim)
            im4=gridding(fileout_stretched_av,hdr3)
            if (DumpAllFitsFiles):
                pf.writeto(fileout_proj,im4, hdr2, overwrite=True)
                
    
            ######################################################################
            # back to sky - rotate
        
            im4drot = ndimage.rotate(im4, -rotangle, reshape=False)
            
            fileout_drotated=re.sub('fullim.fits', 'azim_av_drot.fits', filename_fullim)
            
            if (DumpAllFitsFiles):
                pf.writeto(fileout_drotated,im4drot, hdr2, overwrite=True)
                
            # diff
            diff_im = resamp-im4drot
            fileout_drotated=re.sub('fullim.fits', 'azim_av_drot_diff.fits', filename_fullim)
            if (DumpAllFitsFiles):
                pf.writeto(fileout_drotated,diff_im, hdr2, overwrite=True)


        
        ######################################################################
        # azimuthal profiles

    if ( (DoAzimuthalProfile) or (ProfileExtractRadius  > 0)):

        im_polar_4profiles = np.copy(im_polar)

        rs =  3600.*((np.arange(hdrpolar['NAXIS2'])-hdrpolar['CRPIX2']+1.)*hdrpolar['CDELT2']+hdrpolar['CRVAL2'])
        if (a_min > 0):
            ia_min = np.argmin(np.abs(rs - a_min))
            print( "a_min",a_min," ia_min",ia_min)
            im_polar_4profiles[0:ia_min,:] = 0

        if (a_max > 0):
            ia_max = np.argmin(np.abs(rs - a_max))
            print( "a_max",a_max," ia_min",ia_max)
            im_polar_4profiles[:ia_max,0] = 0

        #im_polar_4profiles[0:20,:] = 0

        #import matplotlib.pyplot as plt
        #plt.imshow(im_polar_4profiles)
        #plt.show()

        Imax_profile=np.amax(im_polar_4profiles,axis=0)
        phis =  (180./np.pi)*((np.arange(hdrpolar['NAXIS1'])-hdrpolar['CRPIX1']+1.)*hdrpolar['CDELT1']+hdrpolar['CRVAL1'])

        (nphis, nrs) = im_polar_4profiles.shape

        #print( "nphis ",nphis,"len phis",len(phis))
        #print( "nrs ",nrs)
        #iphis=np.arange(1,nphis+1)
        #irs=np.arange(1,nrs+1)
        #iiphis,iirs  = np.meshgrid(iphis, irs)
        #rrs =  3600.*(iirs-hdrpolar['CRPIX2']+1)*hdrpolar['CDELT2']+hdrpolar['CRVAL2']
        #pphis =  (180./np.pi)*(iiphis-hdrpolar['CRPIX1']+1)*hdrpolar['CDELT1']+hdrpolar['CRVAL1']


        ivec_rmax=np.argmax(im_polar_4profiles,axis=0)
        Rmax_profile =  3600.*( (ivec_rmax-hdrpolar['CRPIX2']+1.)*hdrpolar['CDELT2']+hdrpolar['CRVAL2'])
        print( "Rmax shape:",Rmax_profile.shape)
        
        
        
        WholeRing=False
        if (ProfileExtractRadius > 0):
            ringrad=ProfileExtractRadius
        elif (WholeRing):
            #Iprof = np.average(im_polar_4profiles,axis=1)
            #ringrad=np.asscalar(Iprof[np.argmax(Iprof)])
            ringrad=np.sum(Rmax_profile*Imax_profile)/np.sum(Imax_profile)
            print( "Rcav ", ringrad)
        else:
            iphimax = np.argmax(Imax_profile)
            print( "iphimax",iphimax)
            iringrad=ivec_rmax[iphimax]
            ringrad=Rmax_profile[iphimax]

        print( "iringrad ",iringrad," ringrad ",ringrad )

        Icyl_profile=im_polar_4profiles[iringrad,:]
        
        
        #np.savetxt(workdir+'rmax_profile.dat', (rs,Rmax_profile))  
        #np.savetxt(workdir+'profile_peak.dat', (rs,Imax_profile))  
        #np.savetxt(workdir+'profile_cyl.dat', (rs,Icyl_profile))  
        
        save_prof = np.zeros((hdrpolar['NAXIS2'],2))
        save_prof[:,0] = phis
        save_prof[:,1] = Rmax_profile
        np.savetxt(workdir+'rmax_profile.dat', save_prof)  
        
        save_prof = np.zeros((hdrpolar['NAXIS2'],2))
        save_prof[:,0] = phis
        save_prof[:,1] = Imax_profile
        np.savetxt(workdir+'profile_peak.dat', save_prof)  

        save_prof = np.zeros((hdrpolar['NAXIS2'],2))
        save_prof[:,0] = phis
        save_prof[:,1] = Icyl_profile
        np.savetxt(workdir+'profile_cyl.dat', save_prof)  

        
        


    
    
    ######################################################################
    # CROSS CHECK INVERSE TRANSFORM

    if (XCheckInv):
        im_x = sp.ndimage.geometric_transform(im_polar,polar2cartesian,
                                              order=1,
                                              output_shape = (im_polar.shape[0], im_polar.shape[1]),
                                              extra_keywords = {'inputshape':im_polar.shape,'fieldscale':fieldscale,
                                                                'origin':(((nx+1)/2)-1,((ny+1)/2)-1)}) 
        
        fileout_stretched_x=re.sub('fullim.fits', 'stretched_x.fits', filename_fullim)
        pf.writeto(fileout_stretched_x,im_x, hdr2, overwrite=True)
        
        hdr3 = deepcopy(hdr2)
        hdr3['CDELT1']=hdr3['CDELT1']/cosi
        fileout_proj_x=re.sub('fullim.fits', 'x_proj.fits', filename_fullim)
        im4_x=gridding(fileout_stretched_x,hdr3)
        pf.writeto(fileout_proj_x,im4_x, hdr2, overwrite=True)
        
        im4_x_drot = ndimage.rotate(im4_x, -rotangle, reshape=False)
        fileout_drotated_x=re.sub('fullim.fits', 'x_drot.fits', filename_fullim)
        pf.writeto(fileout_drotated_x,im4_x_drot, hdr2, overwrite=True)
    
    
        fileout_skydiff=re.sub('fullim.fits', 'x_diff.fits', filename_fullim)
        pf.writeto(fileout_skydiff,resamp-im4_x_drot, hdr2, overwrite=True)

    
    
    ######################################################################
    # profile plotting

    if (PlotRadialProfile):
        import matplotlib.pylab as plt
        import matplotlib.colors as colors
        
        
        # -----------------------------------------------------------
        # nice fonts
        # -----------------------------------------------------------
        matplotlib.rc('font', family='sans-serif') 
        matplotlib.rcParams.update({'font.size': 11})
        
        
        plt.figure(figsize=(7, 3))
        axprofile=plt.subplot(111)
        
        rmax=np.max(rrs)
        
        plt.setp(axprofile.get_xticklabels(),visible=True) #, fontsize=6)
        plt.setp(axprofile.get_yticklabels(),visible=True) #, fontsize=6)
        plt.xlim(0.,rmax)
        #plt.ylim(np.min(Iprof),1.1*np.max(Iprof))
        plt.ylim(-0.1*np.max(Iprof),1.1*np.max(Iprof))
        
        print( np.min(Iprof))
        print( np.max(Iprof))
        
        plt.plot(rrs,rrs*0.,color='black',linewidth=0.1,linestyle='solid')
        
        #plt.plot(rrs,Icutav,color='blue',linewidth=1,linestyle='solid')
        plt.plot(rrs,Iprof,color='grey',linewidth=0.1,linestyle='solid')
        
        plt.fill_between(rrs, Iprof+sIprof, Iprof-sIprof, lw=0.1,color='r', alpha=0.3, interpolate=True, step='mid')
        
        #plt.fill_between(rrs, Iprof+dispIprof, Iprof-dispIprof, lw=0.1,color='b', alpha=0.3, interpolate=True)
        
        plt.ylabel(r'$\tilde{v}_{\phi}(r)$')
    
        plt.xlabel(r'$r$ / arcsec')

        DoStellarMass=True
        if (DoStellarMass):
            distance=114.
            Mstar=2.0
            from astropy import constants as const
            yr = 31558149.8
            RRs=rrs*distance*const.au.value
            vstar = np.sqrt(1.-cosi**2)*1E-3*np.sqrt( const.G.value  * Mstar * const.M_sun.value / RRs )
            vstar[0]=0.
            plt.plot(rrs,vstar,color='darkgreen',linewidth=0.3,linestyle='solid')
            #print( "min vstar",vstar.min())
            #print( "max vstar",vstar.max())
        
    
        fileout_fig=re.sub('fullim.fits', 'fig_profile.pdf', filename_fullim)
        plt.savefig(fileout_fig, bbox_inches='tight')



    ######################################################################

    if (OptimOrient):
        

        chi2=sum(deltaChi2[ia_min:ia_max])
        #print( "chi2: ",chi2)
        return chi2
